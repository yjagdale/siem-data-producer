package produce

import (
	"bufio"
	"github.com/go-baa/pool"
	log "github.com/sirupsen/logrus"
	"net"
	"os"
	"siem-data-producer/producectl/log_utils"
	"siem-data-producer/producectl/tcp_utils"
	"time"
)

func PushLogs(protocol, serverIp string, filePath string, eps int) {
	guard := make(chan string, eps)
	pl, err := pool.New(2, 300, func() interface{} {
		addr, _ := net.ResolveTCPAddr("tcp4", serverIp)
		cli, err := net.DialTCP("tcp4", nil, addr)
		if err != nil {
			log.Fatalf("create client connection error: %v\n", err)
		}
		return cli
	})
	if err != nil {
		panic(err)
	}
	log_utils.Log.Infoln("Connection opened")
	defer pl.Destroy()

	for {
		f, err := os.Open(filePath)
		if err != nil {
			panic(err.Error())
		}
		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			guard <- scanner.Text()
			go tcp_utils.Publish(guard, pl)
		}
		f.Close()
	}
}

func PublicLogsOnce(protocol, serverIp string, filePath string, eps int) {
	pl, err := pool.New(2, eps, func() interface{} {
		addr, _ := net.ResolveTCPAddr("tcp4", serverIp)
		cli, err := net.DialTCP("tcp4", nil, addr)
		if err != nil {
			log.Fatalf("create client connection error: %v\n", err)
		}
		return cli
	})
	if err != nil {
		panic(err)
	}
	defer pl.Destroy()
	file, err := os.Open(filePath)

	if err != nil {
		log_utils.Log.Fatalln(err)
	}

	defer file.Close()

	guard := make(chan string, eps)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		guard <- scanner.Text()
		go func(guard chan string, data string, np *pool.Pool) {
			tcp_utils.Publish(guard, np)
			time.Sleep(1 * time.Second)
		}(guard, scanner.Text(), pl)
	}
}
