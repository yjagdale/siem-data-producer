package main

import (
	continuesPublisher "siem-data-producer/producectl/continues_producer"
	"testing"
)

func TestMain(m *testing.M) {
	continuesPublisher.StartContinuesProducer("10.0.0.1:514", "udp", "/Users/yash/Downloads/paloalto_5k.log", 10, true)
}
