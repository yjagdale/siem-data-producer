package log_utils

import (
	"github.com/sirupsen/logrus"
	easy "github.com/t-tomalak/logrus-easy-formatter"
	"os"
)

var Log *logrus.Logger

func init() {
	lvl, err := logrus.ParseLevel(getEnvOrDefault("LOG_LEVEL", "info"))
	if err != nil {
		lvl = logrus.InfoLevel
	}
	Log = &logrus.Logger{
		Out:   os.Stderr,
		Level: lvl,
		Formatter: &easy.Formatter{
			TimestampFormat: "2006-01-02 15:04:05",
			LogFormat:       "%msg%\n",
		},
	}
}

func getEnvOrDefault(env string, defaultVal string) string {
	if value, exists := os.LookupEnv(env); exists {
		return value
	}
	return defaultVal
}
