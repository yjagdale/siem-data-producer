package tcp_utils

import (
	"github.com/go-baa/pool"
	"net"
	"siem-data-producer/producectl/formatter"
	"siem-data-producer/producectl/log_utils"
	"sync"
	"time"
)

var connections = make(map[string]net.Conn)
var lock = sync.RWMutex{}

func Publish(guard chan string, np *pool.Pool) {
	time.Sleep(1 * time.Second)
	text := <-guard
	c, err := np.Get()
	if err != nil {
		log_utils.Log.Errorln(err)
		return
	}
	formattedList := formatter.FormatLog(text)
	conn := c.(*net.TCPConn)
	_, err = conn.Write([]byte(formattedList + "\n"))
	if err != nil {
		log_utils.Log.Errorln("Error while publishing ", err)
		return
	}
	np.Put(c)
	log_utils.Log.Debugln("Published ", text)
}

func getConnection(ip string, protocol string) (net.Conn, error) {
	if conn, ok := connections[ip]; ok {
		return conn, nil
	} else {
		c, err := net.Dial(protocol, ip)
		if err == nil {
			lock.Lock()
			connections[ip] = c
			lock.Unlock()
		} else {
			return nil, err
		}
		return connections[ip], nil
	}
}
