package attach_simulator

import (
	"github.com/go-baa/pool"
	log "github.com/sirupsen/logrus"
	"net"
	"siem-data-producer/producectl/file_utils"
	"siem-data-producer/producectl/log_utils"
	"siem-data-producer/producectl/produce"
	"siem-data-producer/producectl/tcp_utils"
	"time"
)

func StartAttack(serverIp string, protocol string, logLines []string, eps int, duration int) {
	log_utils.Log.Debugln("EPS", eps)
	pl, err := pool.New(2, eps, func() interface{} {
		addr, _ := net.ResolveTCPAddr("tcp4", "127.0.0.1:8003")
		cli, err := net.DialTCP("tcp4", nil, addr)
		if err != nil {
			log.Fatalf("create client connection error: %v\n", err)
		}
		return cli
	})
	if err != nil {
		panic(err)
	}
	defer pl.Destroy()
	guard := make(chan string, eps)
	for range time.Tick(time.Duration(duration) * time.Second) {
		for _, line := range logLines {
			guard <- line
			tcp_utils.Publish(guard, pl)
			time.Sleep(1 * time.Second)
		}
	}
}

func StartAttackFromFile(serverIp string, protocol string, filePath string, eps int, duration int) {
	file_utils.DisplayStats(filePath)
	log_utils.Log.Infof("Destination: %s", serverIp)
	log_utils.Log.Infof("Protocol: %s", protocol)
	log_utils.Log.Infof("eps: %v", eps)
	log_utils.Log.Infoln("Connection opened")

	for range time.Tick(time.Duration(duration) * time.Second) {
		produce.PushLogs(protocol, serverIp, filePath, eps)
		log_utils.Log.Infoln("Attack published for ", filePath)
	}
}
