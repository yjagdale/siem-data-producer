package continues_publisher

import (
	"siem-data-producer/producectl/file_utils"
	"siem-data-producer/producectl/log_utils"
	"siem-data-producer/producectl/produce"
)

func StartContinuesProducer(serverIp string, protocol string, filePath string, eps int, continues bool) {
	file_utils.DisplayStats(filePath)
	log_utils.Log.Infof("Destination: %s", serverIp)
	log_utils.Log.Infof("Protocol: %s", protocol)
	log_utils.Log.Infof("eps: %v", eps)
	log_utils.Log.Infoln("Connection opened")
	if continues {
		produce.PushLogs(protocol, serverIp, filePath, eps)
	} else {
		produce.PublicLogsOnce(protocol, serverIp, filePath, eps)
	}
}
